var express = require('express');
var hbs = require('hbs');
var fs = require('fs');

var app = express();
var path = require('path');
hbs.registerPartials(__dirname + '/views/partials/');
app.set('view engine', 'hbs');

app.use((req,res,next)=>{
    var now = new Date().toString();
    var log = `${now}:${req.method}:${req.url}`;
    fs.appendFile('serverLog',log + '\n',(error)=>{
        if(error){
            console.log('Unable to append log in server file ');
        }
    });
    next();
});

app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static(__dirname + 'public'));
// app.engine('html', require('hbs').__express);
 app.use(express.static(__dirname + '/vali'));
//app.use(express.static(__dirname + '/bluesky'));

app.get('/',(req, res)=>{
    res.render('home');
    // res.send({
    //     name:'shishir chauhary',
    //     likes:[
    //         'cricket',
    //         'programing',
    //         'coocking'
    //     ]
    // });
});

app.get('/about', (req, res)=> {
    console.log(res);
    res.send('Hi About!');
});

app.get('/admin', (req,res)=>{
    res.render('vali/docs/index');
});

app.get('/bad',(req,res)=>{
    res.send({
        error:"Unable to handle the url"
    });
});
app.listen(5000, ()=>{
    console.log('server is setup on 3030 port');
});